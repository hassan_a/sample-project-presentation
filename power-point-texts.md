# First page

# Content
## Introduction
	A web application to be a reference for APIs implementation which we are going to use for our web applications.Also an internal tool for documentations about Technologies, Frameworks and Modules which our development teams will use.

	 
* Requirements
```
We need to
- Provide our API's references to other developers.
- Provide guides for technologies and modules that we are going to use in our projects for our developers.

```
* Goals
```
Although there are some tools and websites we can use to satisfy our needs like Confluence, GitHub wiki, etc. we decided to build this project because of the following reasons:
- Get familiar with new technologies and modules such as Elasticsearch, Redis cache, RabbitMQ, etc.
- Get familiar with some tools like Gitlab, Trello, Slack, JIRA and Confluence, etc.
- Assemble our team based on Agile methodology and the DevOps cycle.

```
* Methods and Technologies
```
- Agile methodology
- Familiar with microservice architecture
- Implement with Node.JS
- RESTful API
- Angular 
- Caching
- Indexing
- Test-driven development (TDD) programming

```
## Documentations
* Use case
* Activity or Sequence
* ER
* Software Architecture
## Implementation
* Solution Structure
```
- Layer diagram
- Flow of control
app                         → Application sources 
 └ api-models               → Converter objects that transform outside objects (ex: HTTP request payload) to inside objects (ex: Use Case request object)
 └ configs                  → Configs of project like connection string, etc.
 └ controllers              → Route handlers
 └ database                 → ORM and database connection objects
    └ models                → Domain layer (database models)
    └ repository            → Repository implementations
 └ helpers                  → Helper modules
 └ middlewares              → Middlewares of HTTP requests
 └ providers                → Providers of tools (ex: logger)
 └ routes                   → Route handler config
 └ services                 → Application services layer
 └ startup                  → Frameworks, drivers and tools that must config on startup.
 └ validators               → HTTP request object (ex: HTTP request body) validator for controllers
 └ server.js                → Application server entry point
node_modules (generated)    → NPM dependencies
tests                       → Source folder for unit or functional tests

- Project anatomy
```
* Tools and Modules
```
Database: 
    MongoDB: NoSQL database for storing data
    Redis: Dictionary database in ram for caching data
Indexing:
    Elastic search
Font-end implementation:
    Angular 7: A font-end framework for Sinlge page applications (SPA)
Node.js framework:
    Express: A RESTful based framework for Node.js
Code quality:
    Jest: Back-end testing tool for Node.js
    Protractor: End to end testing tool for angular
    ESLint: Linting tool for javascript
    SonarQube: Code quality tool
Documentation:
    JS Doc: Code and module documentaion
    Swagger: RESTful api documentaion
Deployment:
    Docker: Container software
    Nginx: Web server
    PM2: Clustering tool for Node.js
Authentication:
    JWT: Stateless structured token
Queue manager:
    RabbitMQ

```
* Screenshots
# Introduction pages
# Documentations pages
# Implementation pages
# Finish page